//  Add at least 5 members of your fictional group or musical band
db.users.insertMany([
    {
        fisrtName: "John",
        lastName: "Smith",
        email: "johns@mail.com",
        password: "john1234",
        isAdmin: false,
    },
    {
        fisrtName: "Henry",
        lastName: "Cavil",
        email: "henryc@mail.com",
        password: "henry1234",
        isAdmin: false,
    },
    {
        fisrtName: "Joseph",
        lastName: "Carpenter",
        email: "jc@mail.com",
        password: "jc1234",
        isAdmin: false,
    },
    {
        fisrtName: "Luke",
        lastName: "Cage",
        email: "lc@mail.com",
        password: "lc1234",
        isAdmin: false,
    },
    {
        fisrtName: "John",
        lastName: "Romero",
        email: "jr@mail.com",
        password: "jr1234",
        isAdmin: false,
    },
])
// Add 3 new courses in anew course collection
db.courses.insertMany([
     {
         name: "HTML 101",
         price: 1500,
         isActive: false
     },
     {
         name: "CSS 102",
         price: 1300,
         isActive: false
     },
     {
         name: "Hugot 143",
         price: 2000,
         isActive: false
     },

])
// Find all regular/non admin users
db.users.find({isAdmin: false})  
// UPdate first user in the users collection as an admin
db.users.updateOne({},{$set:{isAdmin:true}})
// Update one of the courses as active
db.courses.updateOne({name:"Hugot 143"},{$set:{isActive:true}})
//  Delete all inactive courses
db.courses.deleteMany({isActive:false})

